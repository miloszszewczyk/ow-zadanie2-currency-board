import { Component, OnInit, Input } from '@angular/core';
import { CurrencyService, IRates } from '../currency.service';
import { Observable } from 'rxjs/Rx';

enum BOX_TYPE {
  LATEST = 'LATEST',
  FAVOURITE = 'FAVOURITE'
}

@Component({
  selector: 'app-currency-box',
  templateUrl: './currency-box.component.html',
  styleUrls: ['./currency-box.component.scss']
})
export class CurrencyBoxComponent implements OnInit {
  @Input() type: BOX_TYPE;

  
  data$: Observable<IRates[]>;

  favourites: Set<string> = new Set();

  constructor(private _currSvr: CurrencyService) {
  
  }


  ngOnInit() {
    if (this.type == BOX_TYPE.LATEST) {
      this.data$ = this._currSvr.rates_update$;

      this._currSvr.removeFromFavouritesSubject$.subscribe(base => {
        this.favourites.delete(base);
      })

    } else if (this.type == BOX_TYPE.FAVOURITE) {
      this.data$ = this._currSvr.rates_update$.map(
        element => element.filter(element => this.favourites.has(element.base)
      ));

      this._currSvr.addToFavouritesSubject$.subscribe(base => {
        this.favourites.add(base);
        this._currSvr.refreshData();
      });
    }
  }

  addToFavourites(base) {
    this._currSvr.addToFavouritesSubject$.next(base);
    this.favourites.add(base);
  }

  removeFromFavourites(base) {
    this.favourites.delete(base);
    this._currSvr.removeFromFavouritesSubject$.next(base);
  }

}
