import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable, Subscriber, Subject } from 'rxjs/Rx';

export interface IRates {
  base: string,
  date: string,
  rates: { [key: string]: string }
}

@Injectable()
export class CurrencyService {
  private handleError (error: Response) {
    return Observable.throw(error.json());
  }

  private UPDATE_INTERVAL: number = 30;
  private URL: string = 'https://exchangeratesapi.io/api/latest';

  private obs_collection: Observable<IRates>[] = [];
  private currency_data: IRates[] = [];


  constructor(private http: Http) {
    console.log('Currency service initialized.');

    /* Getting all bases */

    this.http.get(this.URL).map(res => res.json()).catch(this.handleError).subscribe((data: IRates) => {
      let bases: string[] = [];

      bases.push(data.base);

      for (let base of Object.keys(data.rates)) {
        bases.push(base)
      }

      for (let base of bases) {
        this.obs_collection.push(this.http.get(`${this.URL}?base=${base}`).map(res => res.json()).catch(this.handleError));
      }

      this.startLoop();

    });

    this.addToFavouritesSubject$.subscribe(data => {
      this.currencyDataSubject.next(this.currency_data);
    });

    this.removeFromFavouritesSubject$.subscribe(data => {
      this.currencyDataSubject.next(this.currency_data);
    });
  }


  private startLoop() {
    let update_loop = () => {
      /* Parallel requests  */
      Observable.forkJoin(this.obs_collection).subscribe(data => {
        this.currency_data = data;
        this.currencyDataSubject.next(this.currency_data);
      });

      setTimeout(update_loop, this.UPDATE_INTERVAL * 1000);
    }

    update_loop();
  }

  public refreshData() {
    this.currencyDataSubject.next(this.currency_data);
  }

  private currencyDataSubject = new Subject<IRates[]>();
  public rates_update$ = this.currencyDataSubject.asObservable();

  public addToFavouritesSubject$ = new Subject<string>();
  public removeFromFavouritesSubject$ = new Subject<string>();
}
