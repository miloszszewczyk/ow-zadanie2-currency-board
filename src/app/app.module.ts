import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { CurrencyBoxComponent } from './currency-box/currency-box.component';
import { CurrencyService } from './currency.service';
import { ObjectKeysPipe } from './object-keys.pipe';
import { HttpModule } from '@angular/http';


@NgModule({
  declarations: [
    AppComponent,
    CurrencyBoxComponent,
    ObjectKeysPipe
  ],
  imports: [
    BrowserModule,
    HttpModule
  ],
  providers: [CurrencyService],
  bootstrap: [AppComponent]
})
export class AppModule { }
